
# Скрипт на Python для перемещения файлов старше X дней в AWS S3

## Описание

Этот скрипт на Python предназначен для перемещения файлов, которые старше указанного количества дней, из локальной директории в бакет AWS S3. После успешной загрузки файла в S3, файл удаляется из локальной директории.

## Установка и настройка

### 1. Установка Boto3

Убедитесь, что у вас установлен пакет `boto3`, который является SDK для AWS на Python. Установить его можно с помощью следующей команды:

```sh
pip install boto3
```

### 2. Настройка AWS учетных данных

Убедитесь, что ваши AWS учетные данные настроены. Это можно сделать, создав файл `~/.aws/credentials` или используя переменные окружения. Более подробную информацию можно найти в [документации Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html).

### 3. Настройка параметров скрипта

Перед запуском скрипта, измените следующие параметры:

- `local_directory`: Локальная директория, в которой будут искать файлы.
- `s3_bucket_name`: Имя вашего бакета S3.
- `s3_folder_name`: Папка в бакете S3, куда будут загружаться файлы.
- `days_threshold`: Количество дней, файлы старше которых будут перемещены.

## Использование

### Запуск скрипта

После настройки параметров запустите скрипт. Убедитесь, что у вас есть необходимые права для чтения из локальной директории и записи в бакет S3.

```python
import os
import time
import boto3
from datetime import datetime, timedelta

def upload_to_s3(file_path, bucket_name, s3_folder):
    s3 = boto3.client('s3')
    s3_file_path = os.path.join(s3_folder, os.path.basename(file_path))
    try:
        s3.upload_file(file_path, bucket_name, s3_file_path)
        print(f"Uploaded {file_path} to s3://{bucket_name}/{s3_file_path}")
    except Exception as e:
        print(f"Failed to upload {file_path} to S3: {e}")

def move_files_older_than_x_days_to_s3(local_dir, bucket_name, s3_folder, days):
    now = time.time()
    cutoff = now - (days * 86400)

    for filename in os.listdir(local_dir):
        file_path = os.path.join(local_dir, filename)
        if os.path.isfile(file_path):
            file_mtime = os.path.getmtime(file_path)
            if file_mtime < cutoff:
                upload_to_s3(file_path, bucket_name, s3_folder)
                os.remove(file_path)
                print(f"Moved and deleted {file_path}")

if __name__ == "__main__":
    local_directory = "/path/to/local/directory"
    s3_bucket_name = "your-s3-bucket-name"
    s3_folder_name = "your-s3-folder"
    days_threshold = 30  # Files older than 30 days

    move_files_older_than_x_days_to_s3(local_directory, s3_bucket_name, s3_folder_name, days_threshold)
```

## Детальное описание функций

### `upload_to_s3(file_path, bucket_name, s3_folder)`

Загружает указанный файл в бакет S3.

- **Параметры:**
  - `file_path` (str): Путь к файлу, который нужно загрузить.
  - `bucket_name` (str): Имя бакета S3.
  - `s3_folder` (str): Папка в бакете S3, куда будет загружен файл.

### `move_files_older_than_x_days_to_s3(local_dir, bucket_name, s3_folder, days)`

Перемещает файлы старше указанного количества дней из локальной директории в бакет S3 и удаляет их из локальной директории.

- **Параметры:**
  - `local_dir` (str): Локальная директория для поиска файлов.
  - `bucket_name` (str): Имя бакета S3.
  - `s3_folder` (str): Папка в бакете S3, куда будут загружены файлы.
  - `days` (int): Количество дней; файлы старше этого значения будут перемещены.

---

Этот скрипт поможет автоматизировать процесс управления старыми файлами, перемещая их в облачное хранилище S3 и освобождая локальное пространство.